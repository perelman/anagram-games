var dictionary;
var topWords;

const dictionaryPromise = fetch('../wordlist/dictionaries/en_US/en_US.aff')
    .then(response => response.text())
    .then(aff => fetch('../wordlist/dictionaries/en_US/en_US.dic')
        .then(response => response.text())
        .then(dic => {
            dictionary = new Typo('en_US', aff, dic);
        }))
    .catch(error => console.log(error));

const topWordsPromise = fetch('../wordlist/en_50k.txt')
    .then(response => response.text())
    .then(text => {
        topWords = text.trim().split('\n').map(line => {
            var arr = line.split(' ');
            return {'word': arr[0], 'frequency': Number(arr[1])};
        });
    })
    .catch(error => console.log(error));

function isValidWord(str) {
  return dictionary.check(str);
}

