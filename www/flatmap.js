'use strict';

if (!Array.prototype.flatMap) {
    Object.defineProperty(Array.prototype, 'flatMap', {
        value: function(callback) {
            return [].concat(...this.map(callback));
        }
    });
}
