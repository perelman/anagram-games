#!/bin/sh

cat > www/index.html <<DOC
<!doctype html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>A Weird Imagination anagram games</title>
        <link rel="stylesheet" type="text/css" href="/static/css/style.css">
    </head>
    <body>
        <header>
          <nav>
            Back to
            <a href="../">all web apps</a>
          </nav>
        </header>
        <h1>Anagram Games</h1>
        <ul class="apps">
DOC

for app in $(find www -mindepth 1 -maxdepth 1 -type d -exec basename {} \;)
do
    if [ "$app" != "wordlist" -a "$app" != "static" ]
    then
        qrencode -o "../root/www/static/qr/anagram-games-${app}.png" "https://apps.aweirdimagination.net/anagram-games/${app}/"
        if [ -f "www/$app/description" ]
        then
            description=$(cat "www/$app/description")
        else
            description=$app
        fi
        if [ -f "www/$app/offline" ]
        then
            offline=" offline"
        else
            offline=""
        fi
        cat >> www/index.html <<DOC
            <li>
              <div class="app">
                <div class="qr">
                  <a href="/static/qr/anagram-games-${app}.png">
                    <img class="qr" src="/static/qr/anagram-games-${app}.png"/>
                  </a>
                </div>
                <div class="text$offline">
                  <a href="$app/">$app</a>:
                  $description
                </div>
              </div>
            </li>
DOC
    fi
done

cat >> www/index.html <<DOC
        </ul>
        <footer>
          Web apps by
          <a href="https://aweirdimagination.net/~perelman/">Daniel
            Perelman</a>,
          author of the blog <a href="https://aweirdimagination.net/">A Weird
            Imagination</a>.
        </footer>
    </body>
</html>
DOC
